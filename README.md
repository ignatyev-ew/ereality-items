## Первоначальная настройка
В файле **class/getData.php** необходимо прописать доступы к созданной базе данных.
А именно в **7,8** и **17** строке. 
После чего, необходимо выполнить импорт файла **ereality_items.sql** в созданную базу. 
---
## Добавление вещей в базу данных
Для того, чтоб начать взаимодействие с вещами в первую очередь нужно выполнить скрипт [**updateBase.php**]. 
Который добавит отсутствующие или обновит уже существующие вещи в базе данных. 
---
## Просмотр списка вещей
Открыв файл **index.php** в браузере, мы получим список всех имеющихся вещей в базе данных. 
Для того чтоб посмотреть вещи конкретной категории необходимо отправить переменную **category**, которая содержит **ID** категории.
Используя для этих целей **GET** запрос.
---
## Просмотр и улучшение вещей на определенный уровень
Когда вы выбрали вещь из списка, в центральной части экрана появится таблица с текущими характеристиками вещей. 
Если было выбрано оружие, то сверху появиться форма, в которой необходимо указать на какой уровень вы собираетесь улучшить вещь и кнопка самого улучшения.
После клика на данную кнопку, параметры улучшенной вещи появятся в правой части экрана. 
---