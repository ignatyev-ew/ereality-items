<?php


class getData
{
    protected $urlItem = 'http://gapi.ereality.ru/shops_items_all.txt';
    protected $db_user = 'ereality';
    protected $db_passwd = 'Esc2JHT3JkPiwSsG';

    public function updateItemsBase()
    {
        return self::BaseUpdateOrCreate(self::downloadItems());
    }

    public function databaseConnect()
    {
        return new PDO('mysql:host=localhost;dbname=ereality', $this->db_user, $this->db_passwd);
    }

    public function showItems($category = null)
    {
        return self::getItems($category);
    }

    public function getItemInfo($itemID)
    {
        $item = self::databaseConnect()->prepare("SELECT * FROM ereality_items WHERE w_id = ?");
        $item->execute(array($itemID));
        $itemInfo = $item->fetch();
        return $itemInfo['json_params'];
    }

    public function getLevelUp($needLevel, $itemID)
    {
        return self::levelUp($needLevel, $itemID);
    }

    private function downloadItems()
    {
        $itemsPage = file_get_contents($this->urlItem);
        $itemsArrayString = explode("\n", $itemsPage);
        array_pop($itemsArrayString);
        $keyArray = explode("|", $itemsArrayString[0]);
        unset($itemsArrayString[0]);
        foreach ($itemsArrayString as $itemString) {
            $itemsArray = explode("|", $itemString);
            $itemArray[] = array_combine($keyArray, $itemsArray);
        }
        return $itemArray;
    }

    private function BaseUpdateOrCreate($items)
    {
        try {
            $updateCount = 0;
            $insertCount = 0;
            foreach ($items as $item) {
                $check = self::databaseConnect()->prepare("SELECT id  FROM ereality_items WHERE w_id = ? AND ws_id = ?");
                $check->execute(array($item['w_id'], $item['ws_id']));
                $itemID = $check->fetchColumn();
                $itemQuery = [
                    $item['w_id'],
                    $item['ws_id'],
                    $item['w_category'],
                    json_encode($item)
                ];
                if ($itemID) {
                    $itemQuery[] = $itemID;
                    $query = "UPDATE ereality_items SET w_id = ? , ws_id = ?, w_category = ?, json_params = ? WHERE id = ?";
                    $update = self::databaseConnect()->prepare($query);
                    $update->execute($itemQuery);
                    $updateCount++;
                } else {
                    $query = "INSERT INTO ereality_items SET w_id = ? , ws_id = ?, w_category = ?, json_params = ?";
                    $insert = self::databaseConnect()->prepare($query);
                    $insert->execute($itemQuery);
                    $insertCount++;
                }
            }
            $connect = null;
        } catch (PDOException $e) {
            return $e->getMessage();
            die();
        }
        return $result = (object)['update' => $updateCount, 'insert' => $insertCount];
    }

    private function getItems($category = null)
    {
        if (!empty($category)) {
            $items = self::databaseConnect()->prepare("SELECT * FROM ereality_items WHERE w_category = ?");
            $items->execute(array($category));
            while ($item = $items->fetch()) {
                $responce[] = (object)[
                    'json_params' => $item['json_params']
                ];
            }
        } else {
            $items = self::databaseConnect()->query("SELECT * FROM ereality_items");
            while ($item = $items->fetch()) {
                $responce[] = (object)[
                    'json_params' => $item['json_params']
                ];
            }
        }
        return (object)$responce;
    }

    private function levelUp($needLevel, $idItem)
    {
        $item = self::databaseConnect()->prepare("SELECT *  FROM ereality_items WHERE w_id = ?");
        $item->execute(array($idItem));
        $item = $item->fetch();
        $item = json_decode($item['json_params'], true);
        $add_dm = [1.29909107, 1.333871261, 1.320539336, 1.289286619, 1.249460762, 1.217484481, 1.190600999, 1.168218368, 1.14477917, 1.128325977, 1.114792792, 1.103504446, 1.093970319, 1.082273366, 1.074821735, 1.068530142, 1.063152331, 1.058505308, 1.054451298, 1.048280556, 1.044881992, 1.041940503, 1.03936997, 1.037103777, 1.035089976, 1.033287659, 1.029766683, 1.028159993, 1.026741281, 1.025478969, 1.024347772, 1.023327343, 1.022401216, 1.02155598, 1.01939115, 1.018594551, 1.017879863, 1.017234732, 1.016648976, 1.016114178];
        $need_st = [1.08, 1.12, 1.10, 1.09, 1.11, 1.10, 1.09, 1.09, 1.11, 1.13, 1.15, 1.16, 1.17, 1.17, 1.17, 1.17, 1.15, 1.14, 1.14, 1.13, 1.13];
        $add_st = [1.083333333, 1.115384615, 1.103448276, 1.09375, 1.114285714, 1.102564103, 1.093023256, 1.085106383, 1.098039216, 1.089285714, 1.081967213, 1.075757576, 1.070422535, 1.078947368, 1.073170732, 1.068181818, 1.063829787, 1.06, 1.056603774, 1.0625, 1.058823529, 1.055555556, 1.052631579, 1.05, 1.047619048, 1.045454545, 1.049689441, 1.047337278, 1.04519774, 1.043243243, 1.041450777, 1.039800995, 1.038277512, 1.036866359, 1.04, 1.038461538, 1.037037037, 1.035714286, 1.034482759, 1.033333333];
        $add_ac = [1.333333333, 1.375, 1.363636364, 1.333333333, 1.3, 1.269230769, 1.242424242, 1.219512195, 1.2, 1.183333333, 1.169014085, 1.156626506, 1.145833333, 1.136363636, 1.128, 1.120567376, 1.113924051, 1.107954545, 1.102564103, 1.097674419, 1.093220339, 1.089147287, 1.085409253, 1.081967213, 1.078787879, 1.075842697, 1.07310705, 1.070559611, 1.068181818, 1.065957447, 1.063872255, 1.061913696, 1.060070671, 1.058333333, 1.056692913, 1.05514158, 1.053672316, 1.05227882, 1.050955414, 1.04969697];
        $add_mf = [1.166666667, 1.214285714, 1.176470588, 1.15, 1.173913043, 1.148148148, 1.129032258, 1.114285714, 1.128205128, 1.113636364, 1.102040816, 1.092592593, 1.084745763, 1.09375, 1.085714286, 1.078947368, 1.073170732, 1.068181818, 1.063829787, 1.07, 1.065420561, 1.061403509, 1.05785124, 1.0546875, 1.051851852, 1.049295775, 1.053691275, 1.050955414, 1.048484848, 1.046242775, 1.044198895, 1.042328042, 1.040609137, 1.03902439, 1.042253521, 1.040540541, 1.038961039, 1.0375, 1.036144578, 1.034883721];
        $add_hp = [1, 7, 2.571428571, 1.851851852, 1.59, 1.452830189, 1.367965368, 1.310126582, 1.268115942, 1.236190476, 1.211093991, 1.190839695, 1.174145299, 1.160145587, 1.148235294, 1.137978142, 1.129051621, 1.121212121, 1.114272167, 1.108085106, 1.102534562, 1.097526994, 1.092986354, 1.088850174, 1.085066667, 1.081592529, 1.078391275, 1.075431943, 1.072688088, 1.070136986, 1.067759003, 1.065537084, 1.063456346, 1.061503738, 1.059667774, 1.057938299, 1.056306306, 1.054763775, 1.053303543, 1.051919192];
        $add_pr = [2, 1.666666667, 1.5, 1.4, 1.333333333, 1.285714286, 1.25, 1.222222222, 1.2, 1.181818182, 1.166666667, 1.153846154, 1.142857143, 1.133333333, 1.125, 1.117647059, 1.111111111, 1.105263158, 1.1, 1.095238095, 1.090909091, 1.086956522, 1.083333333, 1.08, 1.076923077, 1.074074074, 1.071428571, 1.068965517, 1.066666667, 1.064516129, 1.0625, 1.060606061, 1.058823529, 1.057142857, 1.055555556, 1.054054054, 1.052631579, 1.051282051, 1.05, 1.048780488];
        $add_od_k = [1.483239697, 1.314257481, 1.287592613, 1.259881577, 1.232882801, 1.224744871, 1.206706406, 1.187282003, 1.169410692, 1.160549439, 1.149498024, 1.138364429, 1.127988353, 1.118629163, 1.113170668, 1.107231475, 1.101310653, 1.095655919, 1.090375538, 1.085503179, 1.082367227, 1.079101029, 1.075843436, 1.072676509, 1.069645783, 1.066773739, 1.064068588, 1.062205902, 1.060301299, 1.058399823, 1.056531557, 1.054715911, 1.052964775, 1.051284788, 1.049678971, 1.048517417, 1.047340725, 1.046165471, 1.045003705, 1.043864016];
        $st_up = ['str', 'dex', 'luk', 'int', 'liv', 'sta'];
        $mf_up = ['w_add_kr', 'w_add_ankr', 'w_add_uv', 'w_add_anuv', 'w_add_anar', 'w_add_anbl'];
        $roundArr = ['str', 'dex', 'int', 'luk', 'sta', 'ma', 'hp', 'kr', 'ankr', 'anuv', 'anar', 'anbl', 'uv', 'min_dam', 'max_dam', 'od'];
        for ($i = $item['w_n_level'] - 1; $i < $needLevel - 1; $i++) {
            $item['w_sprice'] *= $add_pr[$i];
            foreach ($st_up as $key => $value) {
                $item['w_n_' . $st_up[$key]] *= $need_st[$i];
                $item['w_add_' . $st_up[$key]] *= $add_st[$i];
                $item[$mf_up[$key]] *= $add_mf[$i];
                $item['w_add_bl_' . $key] *= $add_ac[$i];
            }
            $item['w_add_ma'] *= $add_hp[$i];
            $item['w_add_hp'] *= $add_hp[$i];
            $item['w_add_min_dam'] *= $add_dm[$i];
            $item['w_add_max_dam'] *= $add_dm[$i];
            $item['w_add_dam'] *= $add_ac[$i];
            $item['w_add_od'] *= $add_od_k[$i];
            $item['w_n_massa'] *= 1.1;
        }
        $item['w_n_level'] = $needLevel;
        foreach ($roundArr as $round) {
            $item['w_add_' . $round] = round($item['w_add_' . $round]);
        }
        $item['w_n_massa'] = round($item['w_n_massa']);
        $item['w_add_dam'] = round($item['w_add_dam']);
        $res_k = ($item['w_security'] == 2) ? 1.6 : 1.45;
        $res_k = round($res_k * pow(1.3, $item['w_n_level'] - 9));
        for ($i = 1; $i <= 5; $i++) {
            $orig['w_add_bl_' . $i] = round($orig['w_add_bl_' . $i]);
            $orig['w_add_resist' . $i] = ($orig['w_add_resist' . $i] ? $res_k : 0);
        }
        return (object)$item;
    }
}