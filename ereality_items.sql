SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ereality_items
-- ----------------------------
DROP TABLE IF EXISTS `ereality_items`;
CREATE TABLE `ereality_items`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `w_id` int(16) NOT NULL,
  `ws_id` int(16) NOT NULL,
  `w_category` int(16) NULL DEFAULT NULL,
  `json_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_add` timestamp(0) NULL DEFAULT current_timestamp(0),
  `date_update` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
