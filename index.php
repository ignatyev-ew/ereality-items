<?php
require_once 'class/getData.php';
$getData = new getData();
$items = $getData->showItems($_GET['category']);
?>
<div>
    <div style="width: 20%;float: left">
        <?
            foreach ($items as $item) {
                $itemArr = json_decode($item->json_params);
                echo '<a href="#" class="getInfoAJAX" data-id="' . $itemArr->w_id . '">' . $itemArr->w_name . '</a><br>';
            }
        ?>
    </div>
    <div style="width: 40%;float: left;">
        <p id="result">При клике по предмету, в этой части страницы появится более подробная информация</p>
    </div>
    <div style="width: 40%;float: left;text-align: center">
        <p id="result-levelup">Окно улучшения</p>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script>
    $(".getInfoAJAX").click(function () {
        var id = $(this).data('id');
        $.get("/getItemInfo.php?id=" + id, function (data) {
            $("#result").html(data);
        });
    });
</script>