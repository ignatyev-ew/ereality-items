<?php
require_once 'class/getData.php';
require_once 'lib/itemAttributes.php';
$getData = new getData();
$itemAttributes = new itemAttributes();
$item = $getData->getItemInfo($_GET['id']);
$itemArr = json_decode($item);
echo $itemArr->w_base != 0 ? '<input type="text" id="levelup-input" placeholder="Уровень улучшения"><input type="button" data-id="' . $itemArr->w_id . '" class="levelup-button" style="margin-bottom: 20px;margin-left: 10px;" value="Показать улучшение">' : '';
echo '<table border="1" style="border: 1px solid black;margin-bottom: 20px;"><tr><td>Key</td><td>Value</td><td>Description</td></tr>';
foreach ($itemArr as $key => $item) {
    echo '<tr><td>' . $key . '</td><td>' . $item . '</td><td>' . $itemAttributes->getAttrName($key) . '</td></tr>';
}
echo '</table>';
?>
<script>
    $(".levelup-button").click(function () {
        var level = $('#levelup-input').val();
        if ((level !== '') && (level <= 22) && (level >= 0)) {
            var id = $(this).data('id');
            $.get("/getLevelup.php?id=" + id + "&level=" + level, function (data) {
                $("#result-levelup").html(data);
            });
        } else {
            alert('Уровень улучшения указан не верно');
        }
    });
</script>